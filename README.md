# LeadJet Test Chrome extension

Chrome Extension that injects a container with a random user information in a Linkedin profile.

One the extension is installed, there is a LeadJet icon in the chrome extension area. Click on it and there is a popup with instructions.

## How to Install the Extension

To test the extension, install the dependencies, build or run it in dev mode and then import the extension in chrome using the `Load Unpacked` option.

```bash
$ npm install ## Command to Install the Dependencies

$ npm run dev ## Command to Run the extension in dev mode

$ npm run build ## Command to Create a build
```