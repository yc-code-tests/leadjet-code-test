const API_URL = 'https://randomuser.me/api/';

export const fetchRandomUser = async () => {
  const response = await fetch(API_URL);
  const user = await response.json();

  if (user && user.results && user.results.length > 0) {
    return user.results[0] || null;
  }
  return null;
}