import { fetchRandomUser } from '../api/random-users.api';
import './popup.scss'

const injectUserInDom = (user) => {
  if (user) {
    const port = chrome.extension.connect({
      name: "Communication Channel"
    });

    port.postMessage(JSON.stringify(user));

    chrome.tabs.query({ active: true, currentWindow: true}, function(activeTabs) {
      chrome.tabs.sendMessage(activeTabs[0].id, {
        action: 'inject-user',
        user: JSON.stringify(user)
      });
    });
  }
}

const handleFetchUserError = (err) => {
  alert(`Error fetching the random user: ${err.message}`);
}

document.addEventListener('DOMContentLoaded', () => {
  const button = document.getElementById('injectRandomUserInfoBtn');
  button.addEventListener('click',  () => {
    fetchRandomUser()
      .then(injectUserInDom.bind(this))
      .catch(handleFetchUserError.bind(this));

    return false;
  })
});