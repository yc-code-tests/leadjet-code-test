chrome.runtime.onMessage.addListener(function(request) {
  if(request.action === 'inject-user') {
      const user = JSON.parse(request.user);

      const randomUserTemplate = `
        <img class="ljt-avatar" src="${user.picture.medium}" alt="${user.name.first}-${user.name.last}" />
        <div class="ljt-info">
          <p><strong>${user.name.first} ${user.name.last}</strong></p>
          <p>${user.email}</p>
          <p>${user.phone}</p>
        </div>

        <style rel="stylesheet">
        .ljt-card {
          direction: ${user.nat === 'IR' ? 'rtl' : 'ltr'};
          box-sizing: border-box;
          display: flex;
          padding: 16px;
        }

        .ljt-avatar {
          overflow: hidden;
          box-sizing: border-box;
          width: 64px;
          height: 64px;
          border-radius: 50%;
          object-fit: cover;
        }

        .ljt-info {
          box-sizing: border-box;
          display: flex;
          flex-direction: column;
          padding: 0 16px;
        }
        </style>
      `;

      const randomUserCard = document.getElementById('injected-random-user');
      if (randomUserCard) {
        randomUserCard.innerHTML = randomUserTemplate;
      } else {
        const userSection = document.createElement("section");
        userSection.id = 'injected-random-user';
        userSection.classList.add('artdeco-card', 'ember-view', 'break-words', 'pb3', 'mt4', 'ljt-card');
        userSection.style.padding = '8px 16px';
        userSection.style.boxSizing = 'border-box';
  
        userSection.innerHTML = randomUserTemplate;
  
        const parent = document.querySelector('main');
        parent.insertBefore(userSection, parent.children[1]);
      }
  }
});